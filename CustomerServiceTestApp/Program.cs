﻿using CustomerService;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerServiceTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomerManager manager = new CustomerManager();

            ICustomer customer = manager.CreateCustomer();

            manager.UpdateData(customer, "Name", "Surname", "Address", "Phone");

            manager.AddToRepository(customer);
        }
    }
}
