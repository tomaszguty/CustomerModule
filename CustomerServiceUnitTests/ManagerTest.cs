﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Practices.Unity;
using CustomerService;
using Moq;
using Shared;


namespace CustomerServiceUnitTests
{
    [TestClass]
    public class ManagerTest
    {
        UnityContainer unityContainer = null;

        [TestInitialize()]
        public void init()
        {
            this.unityContainer = new UnityContainer();

            this.unityContainer.RegisterType(typeof(ICustomerRepository), typeof(DefaultCustomerRepository));
        }

        [TestMethod]
        public void create_customer_test()
        {        
            CustomerManager service = new CustomerManager(false);

            try
            {
                service.CreateCustomer();
                Assert.Fail("exception should fire");
            }
            catch (NullReferenceException ex)
            {
                Assert.AreEqual(ex.Message, "customer factory is null");
            }
            catch (Exception)
            {
                Assert.Fail("invalid exception fired");
            }

            Mock<ICustomerFactory> factoryMock = new Mock<ICustomerFactory>();

            Mock<ICustomer> customerMock = new Mock<ICustomer>();

            factoryMock.Setup(f => f.CreateCustomer()).Returns(customerMock.Object);

            service.Factory = factoryMock.Object;

            ICustomer newCustomer = service.CreateCustomer();

            Assert.AreSame(newCustomer, customerMock.Object);
        }

        [TestMethod]
        public void add_to_repository_test()
        {
            CustomerManager service = new CustomerManager(false);

            Mock<ICustomer> customerMock = new Mock<ICustomer>();

            try
            {
                service.AddToRepository(customerMock.Object);
                Assert.Fail("exception should fire");
            }
            catch (NullReferenceException ex)
            {
                Assert.AreEqual(ex.Message, "customer repository is null");
            }
            catch (Exception ex)
            {
                Assert.Fail("invalid exception fired");
            }

            service.Repository = this.unityContainer.Resolve<ICustomerRepository>();

            service.AddToRepository(customerMock.Object);

            Assert.IsTrue(service.Repository.ContainsCustomer(customerMock.Object));
        }

        [TestMethod]
        public void get_customer_test()
        {
            CustomerManager service = new CustomerManager();

            Mock<ICustomer> customerMock = new Mock<ICustomer>();

            customerMock.Setup(c => c.Name).Returns("Mark");

            customerMock.Setup(c => c.Surname).Returns("Twain");

            service.AddToRepository(customerMock.Object);

            ICustomer customer = service.GetCustomer("Mark", "Twain");

            Assert.AreSame(customer, customerMock.Object);
        }

        [TestMethod]
        public void remove_customer_test()
        {
            CustomerManager service = new CustomerManager();

            Mock<ICustomer> customerMock = new Mock<ICustomer>();

            customerMock.Setup(c => c.Name).Returns("Mark");

            customerMock.Setup(c => c.Surname).Returns("Twain");

            service.AddToRepository(customerMock.Object);

            ICustomer customer = service.GetCustomer("Mark", "Twain");

            Assert.AreSame(customer, customerMock.Object);

            bool ret = service.RemoveCustomer(customer);

            Assert.IsTrue(ret);

            ret = service.RemoveCustomer(customer);

            Assert.IsFalse(ret);
        }

        [TestMethod]
        public void update_customer_test()
        {
            CustomerManager service = new CustomerManager();

            ICustomer customer = new DefaultCustomer();

            service.AddToRepository(customer);

            service.UpdateData(customer, "Mark", "Twain", "street 123", "123 456 789");

            Assert.AreEqual("Mark", customer.Name);

            Assert.AreEqual("Twain", customer.Surname);

            Assert.AreEqual("street 123", customer.Address);

            Assert.AreEqual("123 456 789", customer.PhoneNumber);

        }
    }
}
