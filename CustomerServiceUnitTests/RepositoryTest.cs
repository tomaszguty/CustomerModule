﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomerService;
using Moq;
using Shared;

namespace CustomerServiceUnitTests
{
    [TestClass]
    public class RepositoryTest
    {
       

        [TestInitialize()]
        public void init()
        {            
        }

        [TestMethod]
        public void add_customer_test()
        {
            DefaultCustomerRepository repository = new DefaultCustomerRepository();

            Mock<ICustomer> mockCustomer = new Mock<ICustomer>();

            bool contains = repository.ContainsCustomer(mockCustomer.Object);

            Assert.IsFalse(contains);

            repository.AddCustomer(mockCustomer.Object);

            contains = repository.ContainsCustomer(mockCustomer.Object);

            Assert.IsTrue(contains);
        }

         [TestMethod]
        public void get_customer_test()
        {
            DefaultCustomerRepository repository = new DefaultCustomerRepository();

            Mock<ICustomer> mockCustomer = new Mock<ICustomer>();

            try
            {
                ICustomer retCustomer = repository.GetCustomer("Mark", "Twain");
                Assert.Fail("Exception should fire");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is InvalidOperationException);
            }

            mockCustomer.Setup<string>(c => c.Name).Returns("Mark").Verifiable();

            mockCustomer.Setup<string>(c => c.Surname).Returns("Twain").Verifiable();

            repository.AddCustomer(mockCustomer.Object);

            try
            {
                ICustomer retCustomer = repository.GetCustomer("Mark", "Twain");
                Assert.AreSame(mockCustomer.Object, retCustomer);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

            mockCustomer.VerifyGet<string>(c => c.Name);

            mockCustomer.VerifyGet<string>(c => c.Surname);

            repository.AddCustomer(mockCustomer.Object);
             try
             {
                ICustomer retCustomer = repository.GetCustomer("Mark", "Twain");
                Assert.Fail("Exception should fire");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is InvalidOperationException);
            }
        }

         [TestMethod]
         public void remove_customer_test()
         {
             DefaultCustomerRepository repository = new DefaultCustomerRepository();

             Mock<ICustomer> mockCustomer = new Mock<ICustomer>();

             bool ret = repository.RemoveCustomer(mockCustomer.Object);

             Assert.IsFalse(ret);

             repository.AddCustomer(mockCustomer.Object);

             ret = repository.ContainsCustomer(mockCustomer.Object);
             Assert.IsTrue(ret);

             ret = repository.RemoveCustomer(mockCustomer.Object);

             Assert.IsTrue(ret);

             ret = repository.ContainsCustomer(mockCustomer.Object);

             Assert.IsFalse(ret);
         }
    }
}
