﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    /// <summary>
    /// Customer interface
    /// </summary>
    public interface ICustomer
    {
        string Name { get; set; }

        string Surname { get; set; }

        string PhoneNumber { get; set; }

        string Address { get; set; }
    }
}
