﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerService
{
    /// <summary>
    /// Customer factory interface
    /// </summary>
    public interface ICustomerFactory
    {
        /// <summary>
        /// Creates new customer instance
        /// </summary>
        /// <returns></returns>
        ICustomer CreateCustomer();
    }
}
