﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerService
{
    /// <summary>
    /// Customer repository interface
    /// </summary>
    public interface ICustomerRepository
    {
        /// <summary>
        /// Adds customer instance to repository
        /// </summary>
        /// <param name="customer">Customer instance</param>
        void AddCustomer(ICustomer customer);

        /// <summary>
        /// Check whether repository contains customer instance
        /// </summary>
        /// <param name="customer">Customer instance to check</param>
        /// <returns>True if contains, otherwise false</returns>
        bool ContainsCustomer(ICustomer customer);

        /// <summary>
        /// Gets customer from repository based on name and surname
        /// </summary>
        /// <param name="name"></param>
        /// <param name="surname"></param>
        /// <returns>Customer repository if single instance exist, otherwise throws InvalidOperationException</returns>
        ICustomer GetCustomer(string name, string surname);

        /// <summary>
        /// Removes customer instance
        /// </summary>
        /// <param name="customer">Customer instance</param>
        /// <returns>True if success, otherwise false</returns>
        bool RemoveCustomer(ICustomer customer);

    }
}
