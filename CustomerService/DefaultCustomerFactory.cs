﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerService
{
    /// <summary>
    /// Default factory implementation
    /// </summary>
    public class DefaultCustomerFactory : ICustomerFactory
    {
        public ICustomer CreateCustomer()
        {
            return new DefaultCustomer();
        }
    }
}
