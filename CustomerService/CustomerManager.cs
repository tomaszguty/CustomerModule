﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CustomerService
{
    /// <summary>
    /// Customer manager class
    /// </summary>
    public class CustomerManager
    {

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="useDefaults">If true creates default factory and repository instances. 
        /// If false factory and repository leaves null.</param>
        public CustomerManager(bool useDefaults = true)
        {
            if (useDefaults)
            {
                this.Factory = new DefaultCustomerFactory();
                this.Repository = new DefaultCustomerRepository();
            }
        }

        /// <summary>
        /// Customer factory interface
        /// </summary>
        public ICustomerFactory Factory
        {
            get;set;
        }

        /// <summary>
        /// Customer repository interface
        /// </summary>
        public ICustomerRepository Repository
        {
            get;set;
        }

        /// <summary>
        /// Creates new customer using factory
        /// </summary>
        /// <returns>New customer instance</returns>
        public ICustomer CreateCustomer()
        {
            if (this.Factory == null) throw new NullReferenceException("customer factory is null");

            return this.Factory.CreateCustomer();
        }

        /// <summary>
        /// Adds customer to repository
        /// </summary>
        /// <param name="customer">Customer instance</param>
        public void AddToRepository(ICustomer customer)
        {
            if (this.Repository == null) throw new NullReferenceException("customer repository is null");

            this.Repository.AddCustomer(customer);
        }

        /// <summary>
        /// Gets customer from repository based on parameters
        /// </summary>
        /// <param name="name"></param>
        /// <param name="surname"></param>
        /// <returns>Customer instance if exists. Otherwise null</returns>
        public ICustomer GetCustomer(string name, string surname)
        {
            if (this.Repository == null) throw new NullReferenceException("customer repository is null");

            try
            {
                return this.Repository.GetCustomer(name, surname);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Removes cusomer from respository
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>True if successfull, otherwise false</returns>
        public bool RemoveCustomer(ICustomer customer)
        {
              if (this.Repository == null) throw new NullReferenceException("customer repository is null");

              return this.Repository.RemoveCustomer(customer);
        }

        /// <summary>
        /// Updates customer data
        /// </summary>
        /// <param name="customer">Customer instance to update</param>
        /// <param name="name"></param>
        /// <param name="surname"></param>
        /// <param name="address"></param>
        /// <param name="phoneNumber"></param>
        public void UpdateData(ICustomer customer, string name, string surname, string address, string phoneNumber)
        {
            customer.Name = name;
            customer.Surname = surname;
            customer.Address = address;
            customer.PhoneNumber = phoneNumber;
        }

    }
}
