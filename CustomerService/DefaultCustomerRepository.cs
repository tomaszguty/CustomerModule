﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerService
{
    /// <summary>
    /// Default repository implementation
    /// </summary>
    public class DefaultCustomerRepository : ICustomerRepository
    {
        private object locker = new object();

        private List<ICustomer> customers = new List<ICustomer>();

        public void AddCustomer(Shared.ICustomer customer)
        {
            lock (locker)
            {
                customers.Add(customer);
            }
        }

        public bool ContainsCustomer(Shared.ICustomer customer)
        {
            lock (locker)
            {
                return this.customers.Contains(customer);
            }
        }

        public ICustomer GetCustomer(string name, string surname)
        {
            lock (locker)
            {
                return this.customers.Single(c => c.Name == name && c.Surname == surname);
            }
        }

        public bool RemoveCustomer(ICustomer customer)
        {
            lock (locker)
            {
                return this.customers.Remove(customer);
            }
        }
    }
}
